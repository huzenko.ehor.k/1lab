package edu.hneu.mjt.glushkoivan.main;

import edu.hneu.mjt.glushkoivan.dto.BankCard;
import edu.hneu.mjt.glushkoivan.dto.BankCardType;
import edu.hneu.mjt.glushkoivan.dto.User;
import edu.hneu.mjt.glushkoivan.bankImpl.BankImpl;
import edu.hneu.mjt.glushkoivan.ServiceImpl.ServiceImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private static final ArrayList<User> userList = new ArrayList<>();
    private static final ArrayList<BankCard> bankCardList = new ArrayList<>();
    private static final ServiceImpl service = new ServiceImpl();

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        while (true) {
            System.out.println();
            System.out.println("1.  Create a user");
            System.out.println("2.  List of users");
            System.out.println("3.  Create a bank card");
            System.out.println("4.  List of bank cards");
            System.out.println("5.  Subscribe a bank card");
            System.out.println("6.  List of subscribed users");
            System.out.println("7.  Get a subscription by the card's number");
            System.out.println("8.  Get the card information");
            System.out.println("9.  Top up your account");
            System.out.println("10. Get your balance");
            System.out.println("11. Make a transaction");
            System.out.println("0.  Exit");
            var action = scanner.nextInt();
            switch (action) {
                case 1 -> createUser();
                case 2 -> listOfUsers();
                case 3 -> createBankCard(scanner);
                case 4 -> listOfBankCards();
                case 5 -> subscribeTheBankCard(scanner);
                case 6 -> listOfSubscribedUsers();
                case 7 -> getSubscription(scanner);
                case 8 -> getCardInformation(scanner);
                case 9 -> topUpTheCard(scanner);
                case 10 -> getBalance(scanner);
                case 11 -> transfer(scanner);
                default -> {
                    scanner.close();
                    return;
                }
            }
        }
    }

    private static void listOfUsers() {
        AtomicInteger n = new AtomicInteger(0);

        userList.forEach(user -> {
            System.out.println(n + ". " + user.getSurname() + " " + user.getName() + " " + user.getBirthday());
            n.addAndGet(1);
        });
    }

    private static void listOfBankCards() {
        AtomicInteger count = new AtomicInteger(0);

        bankCardList.forEach(card -> {
            System.out.println(count + ". " + card.getNumber() + " " + card.getUser());
            count.addAndGet(1);
        });
    }

    private static void listOfSubscribedUsers() {
        AtomicInteger count = new AtomicInteger(0);

        service.getSubscribeUsers().forEach(user -> {
            System.out.println(count + ". " + user.getSurname() + " " + user.getName() + " " + user.getBirthday());
            count.addAndGet(1);
        });
    }

    public static void createUser() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Your name:");
        var name = scanner.next();
        System.out.println("Your surname:");
        var surname = scanner.next();
        System.out.println("Enter date of your birthday like dd.mm.yyyy:");
        var birthday = scanner.next().split("\\.");

        userList.add(new User(name, surname, LocalDate.of(
                Integer.parseInt(birthday[2]),
                Integer.parseInt(birthday[1]),
                Integer.parseInt(birthday[0])
                ))
        );
        System.out.println("--User created--");
    }

    private static void createBankCard(Scanner scanner) {
        System.out.println("For which user do you want to create a card?:");
        listOfUsers();
        var choice = scanner.nextInt();

        System.out.println("Choose a type of card:");
        System.out.println("1. Debit\t2. Credit");
        BankCardType cardType = BankCardType.DEBIT;

        if (scanner.nextInt() == 2) cardType = BankCardType.CREDIT;
        bankCardList.add(new BankImpl().createBankCard(userList.get(choice), cardType));
        System.out.println("--Card created--");
    }

    private static void subscribeTheBankCard(Scanner scanner) {
        System.out.println("Choose a card for subscribe:");
        listOfBankCards();
        var choice = scanner.nextInt();

        service.subscribe(bankCardList.get(choice));
        System.out.println("--Card subscribed--!");
    }

    private static void getCardInformation(Scanner scanner) {
        System.out.println("Choose your card:");
        listOfBankCards();
        var userCard = scanner.nextInt();
        BankCard card = bankCardList.get(userCard);
        System.out.println(card.toString());
        System.out.println();
    }

    private static void getSubscription(Scanner scanner) {
        System.out.println("Choose a card for get the subscription:");
        listOfBankCards();
        var choice = scanner.nextInt();

        System.out.println(service.getSubscriptionByBankCardNumber(bankCardList.get(choice).getNumber()));
    }

    private static void getBalance(Scanner scanner) {
        System.out.println("Choose a card for get the balance:");
        listOfBankCards();
        var choice = scanner.nextInt();

        BankCard ownerCard = bankCardList.get(choice);
        System.out.println(ownerCard.getBalance());
    }

    private static void topUpTheCard(Scanner scanner) {
        System.out.print("Enter the amount for top up: ");
        var funds = scanner.nextDouble();
        if (funds > 0) {
            System.out.println("Choose the card:");
            listOfBankCards();
            var choice = scanner.nextInt();

            BankCard ownerCard = bankCardList.get(choice);
            ownerCard.popUp(funds);
            System.out.println("--Successful--");
        } else System.out.println("--Error--");
    }

    public static void transfer(Scanner scanner) {
        System.out.print("Enter the amount you want to send: ");
        var funds = scanner.nextDouble();
        if (funds > 0){
            int sender, recipient;

            System.out.println("Choose your card");
            listOfBankCards();
            sender = scanner.nextInt();

            System.out.println("Choose recipient's card");
            listOfBankCards();
            while (true) {
                recipient = scanner.nextInt();
                if (recipient != sender) break;
                else {
                    System.out.println("--Error. Your own card.--");
                }
            }

            BankCard senderCard = bankCardList.get(sender);
            BankCard recipientCard = bankCardList.get(recipient);
            senderCard.makeTransfer(recipientCard, funds);
        }
        else {
            System.out.println("--Error--");
        }
    }
}

