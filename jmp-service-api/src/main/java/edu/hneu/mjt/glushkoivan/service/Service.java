package edu.hneu.mjt.glushkoivan.service;

import edu.hneu.mjt.glushkoivan.dto.BankCard;
import edu.hneu.mjt.glushkoivan.dto.Subscription;
import edu.hneu.mjt.glushkoivan.dto.User;

import java.util.List;
import java.util.Optional;

public interface Service {
    void subscribe(BankCard bankCard);
    Optional<Subscription> getSubscriptionByBankCardNumber(String number);
    List<User> getSubscribeUsers();
}