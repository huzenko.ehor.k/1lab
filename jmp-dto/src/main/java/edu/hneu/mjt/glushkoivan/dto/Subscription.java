package edu.hneu.mjt.glushkoivan.dto;

import java.time.LocalDate;

public class Subscription {
    private final String bankCard;
    private final String cardType;
    private final LocalDate startDate;

    public Subscription(String bankCard, String cardType, LocalDate startDate) {
        this.bankCard = bankCard;
        this.cardType = cardType;
        this.startDate = startDate;
    }

    @Override
    public String toString() {
        return "Subscription: " + "bankCard=" + bankCard + ", cardType=" + cardType + ", startDate=" + startDate;
    }
}