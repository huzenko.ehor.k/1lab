package edu.hneu.mjt.glushkoivan.dto;

import java.util.concurrent.atomic.AtomicLong;

public class BankCard {
    private static final AtomicLong COUNTER = new AtomicLong(0);

    protected Double balance;
    protected String cardNumber;
    private final Long cardCounter;
    private final User user;
    private final String cardType;

    public BankCard(User user, String cardType) {
        cardCounter = COUNTER.getAndIncrement();
        this.cardNumber = setNumber();
        this.cardType = cardType;
        this.user = user;
        this.balance = 0.0;


    }

    public String getBalance() {
        return "Main: " + balance.toString();
    }

    public String getNumber() {
        return cardNumber;
    }

    public String getCardType() {
        return cardType;
    }

    public User getUser() {
        return user;
    }

    public String setNumber() {
        String cardNumberPattern = "4149000000000000";
        cardNumber = String.valueOf((Long.parseLong(cardNumberPattern) + cardCounter));
        return cardNumber;
    }

    public void popUp(Double funds) {
        balance += funds;
    }

    public void makeTransfer(BankCard recipient, Double payment) {
        if (payment <= balance) {
            recipient.balance += payment;
            balance -= payment;
            System.out.println("--Success--");
        } else {
            System.out.println("--Error--");
        }
    }


    @Override
    public String toString() {
        return "Bank card: " + "Number='" + cardNumber + '\'' +
                ", Card Type=" + getCardType() +
                ", Balance=" + getBalance() +
                ", Owner={ " + user + " }";
    }
}