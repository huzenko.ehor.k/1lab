package edu.hneu.mjt.glushkoivan.dto;

import java.time.LocalDate;

public class User {

    private String name, surname;
    private LocalDate birthday;

    public User(String name, String surname, LocalDate birthday){
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    private void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getBirthday(){
        return birthday;
    }

    @Override
    public String toString() {
        return "Name= '" + getName() + '\'' + ", Surname= '" + getSurname() + '\'' + ", Birthday= " + getBirthday();
    }
}
