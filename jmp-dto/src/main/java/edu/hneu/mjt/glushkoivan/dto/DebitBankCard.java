package edu.hneu.mjt.glushkoivan.dto;

public class DebitBankCard extends BankCard {
    public DebitBankCard(User user, String cardType) {
        super(user, cardType);
    }
}
