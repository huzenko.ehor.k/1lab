package edu.hneu.mjt.glushkoivan.dto;

import java.util.Scanner;

public class CreditBankCard extends BankCard {

    protected Double creditLimit;

    public CreditBankCard(User user, String cardType) {
        super(user, cardType);
        creditLimit = 10000.0;
    }

    public String getCreditLimit() {
        return "Credit: " + creditLimit.toString();
    }

    @Override
    public String getBalance() {
        return balance + " " + getCreditLimit();
    }

    @Override
    public void makeTransfer(BankCard recipient, Double payment) {
        Scanner scanner = new Scanner(System.in);

        String transactionType = "";
        if (payment <= balance) {
            transactionType = "MainBalance";

        } else {
            System.out.println("Credit balance " + creditLimit);
            if (payment <= creditLimit + balance) {
                System.out.println("Do you want to use credit money?");
                System.out.println("1. Yes\t2. No");
                var action = scanner.nextInt();

                switch (action) {
                    case 1 -> transactionType = "MainAndCreditBalance";
                    case 2 -> transactionType = "CancelTransaction";
                }
            } else {
                System.out.println("--Money isn't enough--");
            }
        }

        switch (transactionType) {
            case "MainBalance" -> {
                recipient.balance += payment;
                balance -= payment;
                System.out.println("--Success--");
            }
            case "MainAndCreditBalance" -> {
                recipient.balance += payment;
                payment -= balance;
                balance = 0.0;
                creditLimit -= payment;
                System.out.println("--Success--");
            }
            case "CancelTransaction" -> {
                System.out.println("--Cancel--");
            }
        }
    }
}