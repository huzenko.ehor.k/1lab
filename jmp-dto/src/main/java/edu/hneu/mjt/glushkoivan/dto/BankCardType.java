package edu.hneu.mjt.glushkoivan.dto;

public enum BankCardType {
    CREDIT,
    DEBIT
}
