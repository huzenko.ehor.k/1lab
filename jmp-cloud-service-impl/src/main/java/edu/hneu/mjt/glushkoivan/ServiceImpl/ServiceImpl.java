package edu.hneu.mjt.glushkoivan.ServiceImpl;

import edu.hneu.mjt.glushkoivan.dto.BankCard;
import edu.hneu.mjt.glushkoivan.dto.Subscription;
import edu.hneu.mjt.glushkoivan.dto.User;
import edu.hneu.mjt.glushkoivan.service.Service;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class ServiceImpl implements Service {
    private final Map<String, Subscription> cardToSubscription = new HashMap<>();
    private final Map<User, Subscription> userToSubscription = new HashMap<>();

    @Override
    public void subscribe(BankCard bankCard) {
        var subscription = new Subscription(bankCard.getNumber(), bankCard.getCardType(), LocalDate.now());
        userToSubscription.put(bankCard.getUser(), subscription);
        cardToSubscription.put(bankCard.getNumber(), subscription);
    }

    @Override
    public Optional<Subscription> getSubscriptionByBankCardNumber(String number) {
        return Optional.of(cardToSubscription.get(number));
    }

    @Override
    public List<User> getSubscribeUsers() {
        return userToSubscription.keySet().stream().toList();
    }
}
