package edu.hneu.mjt.glushkoivan.bankImpl;

import edu.hneu.mjt.glushkoivan.dto.*;
import edu.hneu.mjt.glushkoivan.bankApi.Bank;

public class BankImpl implements Bank {
    @Override
    public BankCard createBankCard(User user, BankCardType cardType) {
        BankCard newCard = null;
        switch (cardType) {
            case CREDIT -> newCard = new CreditBankCard(user, "Credit Card");
            case DEBIT -> newCard = new DebitBankCard(user, "Debit Card");
        }
        return newCard;
    }
}

