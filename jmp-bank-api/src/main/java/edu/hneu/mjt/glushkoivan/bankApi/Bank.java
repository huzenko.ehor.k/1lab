package edu.hneu.mjt.glushkoivan.bankApi;


import edu.hneu.mjt.glushkoivan.dto.BankCard;
import edu.hneu.mjt.glushkoivan.dto.BankCardType;
import edu.hneu.mjt.glushkoivan.dto.User;

public interface Bank {
    public BankCard createBankCard(User user, BankCardType cardType);
}
